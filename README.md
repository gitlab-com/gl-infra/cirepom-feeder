# CIRepoM Feeder

A simple script that feeds [CIRepoM](https://gitlab.com/gitlab-com/gl-infra/cirepom)
a list of repositories in order to rebalance repository storages in GitLab.com.

The script is expected to run as a CI job in ops.gitlab.net, so it can access
Thanos.

## Expected Environment Variables

* `THANOS_HOST`: Required. The host of `thanos-query` instance, in the form of `IP:Port`.
* `TARGET_INSTANCE`: Required. Can be `gitlab.com` or `staging.gitlab.com`
* `TARGET_API_PRIVATE_TOKEN`: Required. A private token of an admin user on the target instance.
* `OPS_API_PRIVATE_TOKEN`: Required. A private token of a user on ops.gitlab.net.
* `EXCLUDED_SHARDS`: Optional. A comma-separated list of repository shard names that shouldn't be considered
  rebalancing, either from or to. Example: `file-hdd-01,file-marquee-03`
* `DRY_RUN`: Optional. Just prints out actions without creating or updating resources.
  It also dumps a marshalled array of source shard, destination shard and projects so they can be used directly
  without re-fetching.
